Faiq Rosadi Arridho <br>
222011636 <br>
2KS2 <br>

Link to heroku deploy: https://apippk4.herokuapp.com/

<b>GET</b>
![](/docs/swagger-get-product.png)

<b>GET by Id</b>
![](/docs/swagger-get-product-by-id.png)

<b>POST</b><br>
Proses tambah data
![](/docs/swagger-post.png)
<br>
Proses cek data yang ditambahkan
![](/docs/swagger-cek-post.png)

<b>PUT</b><br>
Proses edit data
![](/docs/swagger-put.png)
<br>
Proses cek data yang diedit
![](/docs/swagger-cek-put.png)

<b>DELETE</b><br>
Proses hapus data
![](/docs/swagger-delete.png)
<br>
Proses cek data yang dihapus
![](/docs/swagger-cek-delete.png)


